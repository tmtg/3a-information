%%exif_3A_v2.0
function results = exif_3A_fun(str_1,res,exe_path)
str = [str_1,' = '];
[FileName_pic,PathName_pic] = uigetfile('*.jpg','MultiSelect','on');
addpath(PathName_pic);
addpath('C:\Program Files\Qualcomm\Chromatix');
cmd_path = [exe_path,'\'];
cd(cmd_path);

filename = FileName_pic;
path_3a_infor = PathName_pic;
for i = 1:length(filename)
    read_filename = [PathName_pic,char(filename(i))];
    command = ['Chromatix.exe -3ai -i ',read_filename,' -o ',path_3a_infor];
    dos(command);
    
end

fol_pat = path_3a_infor;
cd(fol_pat);
file_list =  dir(fullfile(fol_pat,'*.txt')); % read log
filenames={file_list.name}'; %
filelength = length(file_list);
results = cell(filelength+1,2);
results(1,:) = {'filename',str};
for i = 1:filelength
    filename = char(filenames(i));
    file = fopen(filename,'r');   
    A = fread(file,'char=>char');
    B = A';
    expression = [str,'(-?\d+(\.\d+)?)'];
    [tokens1] = regexp(B,expression, 'tokens'); %%%%%%% for str
    results(i+1,1) = filenames(i);
    results(i+1,2) =  tokens1{1}(1);

end

res_1 = [res,'.xlsx'];
 xlswrite(res_1,results);

